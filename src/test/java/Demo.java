import com.bean.FootballInfo;
import com.bean.GameStateInfo;
import com.bean.ReciveFootballInfo;
import com.service.action.CheckLsStateService;
import com.service.action.ClickService;
import com.service.action.EnterFootBallHome;
import com.service.getdata.FootballInfoService;
import com.service.getdata.GameStateInfoService;
import com.service.getdata.ReciveFootBallInfoService;
import com.service.webdriver.Browser;
import com.service.webdriver.ElementAction;
import com.utils.Log;
import com.utils.TimeUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by Administrator on 2017/3/17.
 */
public class Demo extends Browser{
    Log log=new Log(this.getClass());
    ClickService clickService=new ClickService();
    GameStateInfoService gameStateInfoService=new GameStateInfoService();
    ElementAction action=new ElementAction();
    public static void main(String[] args){
//        new Demo().test1();
//        new Demo().test2("中国甲级联赛","杭州绿城","新疆天山");
         new Demo().test3();
//        new Demo().test5();
//        new Demo().test5();
    }

    /**
     * 通过扫描当前存在的联赛情况，自动并发抓取所有比赛信息--多实例并发
     */
    public void  test1()
    {
        //启动浏览器
        setWebDriver();
        //进入足球主页
        new EnterFootBallHome().enterHome();
        //获取当前所有足球赛事信息
        ReciveFootBallInfoService reciveFootBallInfoService=new ReciveFootBallInfoService();
        ReciveFootballInfo reciveFootballInfo= reciveFootBallInfoService.getReciveFootBallInfo();
        if (reciveFootballInfo.getMsg().equals("")){
            List<FootballInfo> footballInfos=reciveFootballInfo.getListLianSaiInfo();
            log.info("当前有"+footballInfos.size()+"场比赛在进行直播");
//

            //创建并发线程池
            ThreadPoolExecutor threadPoolExecutor=new ThreadPoolExecutor(5,10,1, TimeUnit.MINUTES,new LinkedBlockingDeque<>(),new ThreadPoolExecutor.DiscardPolicy());
            log.info("并发进行比赛数据抓取");
            /**
             * 将每场比赛添加到线程任务，并发读取数据footballInfos.size()
             */
            for (int i=0;i<3;i++)
            {
                FootballInfo footballInfo=footballInfos.get(i);
                log.info("开始第"+i+"场赛事数据抓取");
                threadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        //执行一场球赛信息抓取
                        long startTime=System.currentTimeMillis();
                        //启动浏览器
                        setWebDriver();
                        //进入足球主页
                        new EnterFootBallHome().enterHome();
//                        //点击比赛现场
//                        clickService.clickMatchLive();
                        GameStateInfo gameStateInfo=new GameStateInfo();
                        ElementAction action=new ElementAction();
                        CheckLsStateService checkLsStateService=new CheckLsStateService();
                        for (int i=0;true;i++){
                            if (!checkLsStateService.checkLsIsOver()){//检查是否有联赛
                                gameStateInfo=gameStateInfoService.getSingleGameStateInfo(footballInfo);
                                System.out.println(gameStateInfo.toString());
                                long endTime=System.currentTimeMillis();
                                long totalTime=endTime-startTime;
                                log.info("持续运行"+ TimeUtil.formatMsTime(totalTime));
                            }else{
                                gameStateInfo.setMsg("无联赛在进行");
                                log.info(gameStateInfo.getMsg());
                                long endTime=System.currentTimeMillis();
                                long totalTime=endTime-startTime;
                                log.info("持续运行"+TimeUtil.formatMsTime(totalTime));
                            }

                        }
                    }
                });
            }
        }


    }

    /**
     * 输入 lsname，zdname，kdname 获取联赛信息
     */
    public void test2(String lsName,String zdName,String kdName)
    {
        FootballInfo footballInfo=new FootballInfo();
        footballInfo.setLsname(lsName);
        footballInfo.setZdname(zdName);
        footballInfo.setKdname(kdName);
        //执行一场球赛信息抓取
        long startTime=System.currentTimeMillis();
        //启动浏览器
        setWebDriver();
        //进入足球主页
        new EnterFootBallHome().enterHome();
        FootballInfoService footballInfoService=new FootballInfoService();
        System.out.println(footballInfoService.getSingleFootBallInfo(footballInfo));;
    }

    /**
     * 获取当前所有联赛信息
     */
    public void test3(){
        //执行一场球赛信息抓取
        long startTime=System.currentTimeMillis();
        //启动浏览器
        setWebDriver();
        //进入足球主页
        new EnterFootBallHome().enterHome();
        ReciveFootBallInfoService reciveFootBallInfoService=new ReciveFootBallInfoService();
       ReciveFootballInfo reciveFootballInfo= reciveFootBallInfoService.getReciveFootBallInfo();
        System.out.println(reciveFootballInfo.getMsg());
        List<FootballInfo> footballInfos=reciveFootballInfo.getListLianSaiInfo();
        for (int i=0;i<footballInfos.size();i++){
            System.out.println("第"+(i+1)+"场比赛");
            System.out.println(footballInfos.get(i));
        }
    }

    /**
     * 非并发实时获取
     */
    public void test4(){
        long startTime=System.currentTimeMillis();
        //启动浏览器
        setWebDriver();
        //进入足球主页
        new EnterFootBallHome().enterHome();
        //点击比赛现场
        clickService.clickMatchLive();
        GameStateInfo gameStateInfo=new GameStateInfo();
        ElementAction action=new ElementAction();
        CheckLsStateService checkLsStateService=new CheckLsStateService();
        for (int i=0;true;i++){
            if (!checkLsStateService.checkLsIsOver()){//检查是否有联赛
                gameStateInfo=gameStateInfoService.getGameStateInfo();
                System.out.println(gameStateInfo.toString());
                long endTime=System.currentTimeMillis();
                long totalTime=endTime-startTime;
                log.info("持续运行"+TimeUtil.formatMsTime(totalTime));
            }else{
                gameStateInfo.setMsg("无联赛在进行");
                log.info(gameStateInfo.getMsg());
                long endTime=System.currentTimeMillis();
                long totalTime=endTime-startTime;
                log.info("持续运行"+TimeUtil.formatMsTime(totalTime));

            }

        }
    }

    /**
     * 单实例多标签并发
     */
    public void  test5()
    {
        //启动浏览器
        setWebDriver();
        //进入足球主页
        new EnterFootBallHome().enterHome();
        //获取当前所有足球赛事信息
        ReciveFootBallInfoService reciveFootBallInfoService=new ReciveFootBallInfoService();
        ReciveFootballInfo reciveFootballInfo= reciveFootBallInfoService.getReciveFootBallInfo();
        if (reciveFootballInfo.getMsg().equals("")){
            List<FootballInfo> footballInfos=reciveFootballInfo.getListLianSaiInfo();
            log.info("当前有"+footballInfos.size()+"场比赛在进行直播");
            //创建并发线程池
            ThreadPoolExecutor threadPoolExecutor=new ThreadPoolExecutor(5,10,1, TimeUnit.MINUTES,new LinkedBlockingDeque<>(),new ThreadPoolExecutor.DiscardPolicy());
            log.info("并发进行比赛数据抓取");
            /**
             * 将每场比赛添加到线程任务，并发读取数据footballInfos.size()
             */

            for (int i=30,j=0;i<34;i++,j++)
            {
                action.sleep(500);
                //每个任务开新标签进行数据抓取
                action.openNewTab(j+1);
                ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
                log.info("当前窗口数量"+tabs.size());
                driver.switchTo().window(tabs.get(j+1));//switches to new tab
                log.info("切换到第"+(j+1)+"个窗口");
                FootballInfo footballInfo=footballInfos.get(i);
                log.info("开始第"+(j+1)+"场赛事数据抓取");
                threadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        //执行一场球赛信息抓取
                        long startTime=System.currentTimeMillis();
                        //启动浏览器
//                        setWebDriver();
                        //进入足球主页
                        new EnterFootBallHome().enterHomeForNewTab();
//                        //点击比赛现场
//                        clickService.clickMatchLive();
                        GameStateInfo gameStateInfo=new GameStateInfo();
                        ElementAction action=new ElementAction();
                        CheckLsStateService checkLsStateService=new CheckLsStateService();
                        for (int i=0;true;i++){
                            if (!checkLsStateService.checkLsIsOver()){//检查是否有联赛
                                gameStateInfo=gameStateInfoService.getSingleGameStateInfo(footballInfo);
                                System.out.println(gameStateInfo.toString());
                                long endTime=System.currentTimeMillis();
                                long totalTime=endTime-startTime;
                                log.info("持续运行"+ TimeUtil.formatMsTime(totalTime));
                            }else{
                                gameStateInfo.setMsg("无联赛在进行");
                                log.info(gameStateInfo.getMsg());
                                long endTime=System.currentTimeMillis();
                                long totalTime=endTime-startTime;
                                log.info("持续运行"+TimeUtil.formatMsTime(totalTime));
                            }

                        }
                    }
                });
            }
        }else {
            log.info("当前无赛事");
            action.sleep(5000);
        }
    }
    /**
     * 多浏览器实例并发
     */
    public void  test6()
    {
        //启动浏览器
        setWebDriver();
        //进入足球主页
        new EnterFootBallHome().enterHome();
        //获取当前所有足球赛事信息
        ReciveFootBallInfoService reciveFootBallInfoService=new ReciveFootBallInfoService();
        ReciveFootballInfo reciveFootballInfo= reciveFootBallInfoService.getReciveFootBallInfo();
        if (reciveFootballInfo.getMsg().equals("")){
            List<FootballInfo> footballInfos=reciveFootballInfo.getListLianSaiInfo();
            log.info("当前有"+footballInfos.size()+"场比赛在进行直播");
            //创建并发线程池
            ThreadPoolExecutor threadPoolExecutor=new ThreadPoolExecutor(5,10,1, TimeUnit.MINUTES,new LinkedBlockingDeque<>(),new ThreadPoolExecutor.DiscardPolicy());
            log.info("并发进行比赛数据抓取");
            /**
             * 将每场比赛添加到线程任务，并发读取数据footballInfos.size()
             */
            for (int i=0;i<3;i++)
            {
                action.sleep(500);
                //每个任务开新标签进行数据抓取
//                action.openNewTab(i+1);
//                ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
//                log.info("当前窗口数量"+tabs.size());
//                driver.switchTo().window(tabs.get(i+1));//switches to new tab
//                log.info("切换到第"+(i+1)+"个窗口");
                FootballInfo footballInfo=footballInfos.get(i);
                log.info("开始第"+(i+1)+"场赛事数据抓取");
                threadPoolExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        //执行一场球赛信息抓取
                        long startTime=System.currentTimeMillis();
//                        启动浏览器
                        setWebDriver();
                        //进入足球主页
                        new EnterFootBallHome().enterHome();
//                        //点击比赛现场
//                        clickService.clickMatchLive();
                        GameStateInfo gameStateInfo=new GameStateInfo();
                        ElementAction action=new ElementAction();
                        CheckLsStateService checkLsStateService=new CheckLsStateService();
                        for (int i=0;true;i++){
                            if (!checkLsStateService.checkLsIsOver()){//检查是否有联赛
                                gameStateInfo=gameStateInfoService.getSingleGameStateInfo(footballInfo);
                                System.out.println(gameStateInfo.toString());
                                long endTime=System.currentTimeMillis();
                                long totalTime=endTime-startTime;
                                log.info("持续运行"+ TimeUtil.formatMsTime(totalTime));
                            }else{
                                gameStateInfo.setMsg("无联赛在进行");
                                log.info(gameStateInfo.getMsg());
                                long endTime=System.currentTimeMillis();
                                long totalTime=endTime-startTime;
                                log.info("持续运行"+TimeUtil.formatMsTime(totalTime));
                            }

                        }
                    }
                });
            }
        }else {
            log.info("当前无赛事");
            action.sleep(5000);
        }
    }
}
