package test;

import com.bean.FootballInfo;
import com.bean.GameStateInfo;
import com.service.action.CheckLsStateService;
import com.service.action.ClickService;
import com.service.action.EnterFootBallHome;
import com.service.getdata.FootballInfoService;
import com.service.getdata.GameStateInfoService;
import com.service.getdata.ReciveFootBallInfoService;
import com.service.webdriver.Browser;
import com.service.webdriver.ElementAction;
import com.utils.Log;
import com.utils.ScreenShot;
import com.utils.TimeUtil;
import org.openqa.selenium.StaleElementReferenceException;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/3/15.
 */
public class Footbolltest extends Browser{
    Log log=new Log(this.getClass());
    FootballInfoService footballInfoService=new FootballInfoService();
    GameStateInfoService gameStateInfoService=new GameStateInfoService();
    ReciveFootBallInfoService reciveFootBallInfoService=new ReciveFootBallInfoService();
    ClickService clickService=new ClickService();
    public static void main(String[] args){
//        new Footbolltest().holdState();
    }
    public void test()
    {
        //启动浏览器
        setWebDriver();
        //进入足球主页
        new EnterFootBallHome().enterHome();
        List<FootballInfo> footballInfos=new ArrayList<>();
        CheckLsStateService checkLsStateService=new CheckLsStateService();
        for (int i=0;i<1000;i++){
            try {
                footballInfos= reciveFootBallInfoService.getReciveFootBallInfo().getListLianSaiInfo();
            }catch (StaleElementReferenceException e){
                footballInfos= reciveFootBallInfoService.getReciveFootBallInfo().getListLianSaiInfo();
            }
            for (int j=0;j<footballInfos.size();j++)
            {
                System.out.println(footballInfos.get(j).toString());
            }

        }
    }
    public void test2()
    {
        long startTime=System.currentTimeMillis();
        //启动浏览器
        setWebDriver();
        //进入足球主页
        new EnterFootBallHome().enterHome();
        //点击比赛现场
        clickService.clickMatchLive();
        GameStateInfo gameStateInfo=new GameStateInfo();
        ElementAction action=new ElementAction();
        CheckLsStateService checkLsStateService=new CheckLsStateService();
        for (int i=0;true;i++){
                if (!checkLsStateService.checkLsIsOver()){//检查是否有联赛
                    gameStateInfo=gameStateInfoService.getGameStateInfo();
                    System.out.println(gameStateInfo.toString());
                    long endTime=System.currentTimeMillis();
                    long totalTime=endTime-startTime;
                    log.info("持续运行"+TimeUtil.formatMsTime(totalTime));
                }else{
                    gameStateInfo.setMsg("无联赛在进行");
                    log.info(gameStateInfo.getMsg());
                    long endTime=System.currentTimeMillis();
                    long totalTime=endTime-startTime;
                    log.info("持续运行"+TimeUtil.formatMsTime(totalTime));

                }

            }

    }
   @Test
    public void holdState(){
        long startTime=0;
        try {
            startTime=System.currentTimeMillis();
            new Footbolltest().test2();
        }catch (Exception e){
            e.printStackTrace();
            long endTime=System.currentTimeMillis();
            long totalTime=endTime-startTime;
            System.out.println("出现异常事件"+totalTime);
            ScreenShot screenShot=new ScreenShot(driver);
            long time=System.currentTimeMillis();
            screenShot.setscreenName(TimeUtil.formatDate(time)+".jpg");
            screenShot.takeScreenshot();
            driver.close();
            driver.quit();
            holdState();
        }
    }
}
