package com.bean.skySport;

public class Ls {
    /** 联赛名称 */
    private String lsName;
    /** 联赛时间基础 */
    private String lsTimeBase;
    private Team team;

    public String getLsName() {
        return lsName;
    }

    public void setLsName(String lsName) {
        this.lsName = lsName;
    }

    public String getLsTimeBase() {
        return lsTimeBase;
    }

    public void setLsTimeBase(String lsTimeBase) {
        this.lsTimeBase = lsTimeBase;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }
}
