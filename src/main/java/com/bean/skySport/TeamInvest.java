package com.bean.skySport;

import java.util.List;

public class TeamInvest {
   private Ls ls;
   private Team team;
   private GameInfo gameInfo;
   private List<PK> pkList;
    /** 第几页 */
    private int webPageIndex=0;

    public Ls getLs() {
        return ls;
    }

    public void setLs(Ls ls) {
        this.ls = ls;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public GameInfo getGameInfo() {
        return gameInfo;
    }

    public void setGameInfo(GameInfo gameInfo) {
        this.gameInfo = gameInfo;
    }

    public List<PK> getPkList() {
        return pkList;
    }

    public void setPkList(List<PK> pkList) {
        this.pkList = pkList;
    }

    public int getWebPageIndex() {
        return webPageIndex;
    }

    public void setWebPageIndex(int webPageIndex) {
        this.webPageIndex = webPageIndex;
    }
}
