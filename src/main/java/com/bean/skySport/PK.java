package com.bean.skySport;

import java.math.BigDecimal;

public class PK {
    /** 上半场、全场 */
    private String isFull;
    /** 让球盘,大小盘 */
    private String teamType;
    /** 主队赔率类型 */
    private String zdPLLX;
    /** 客队赔率类型 */
    private String kdPLLX;
    /** 主队赔率 */
    private float zdplBD;
    /** 客队赔率 */
    private float kdplBD;
    /** 赔率类型 */
    private BigDecimal pllxBD;//可以不用

    public String getIsFull() {
        return isFull;
    }

    public void setIsFull(String isFull) {
        this.isFull = isFull;
    }

    public String getTeamType() {
        return teamType;
    }

    public void setTeamType(String teamType) {
        this.teamType = teamType;
    }

    public String getZdPLLX() {
        return zdPLLX;
    }

    public void setZdPLLX(String zdPLLX) {
        this.zdPLLX = zdPLLX;
    }

    public String getKdPLLX() {
        return kdPLLX;
    }

    public void setKdPLLX(String kdPLLX) {
        this.kdPLLX = kdPLLX;
    }

    public float getZdplBD() {
        return zdplBD;
    }

    public void setZdplBD(float zdplBD) {
        this.zdplBD = zdplBD;
    }

    public float getKdplBD() {
        return kdplBD;
    }

    public void setKdplBD(float kdplBD) {
        this.kdplBD = kdplBD;
    }

    public BigDecimal getPllxBD() {
        return pllxBD;
    }

    public void setPllxBD(BigDecimal pllxBD) {
        this.pllxBD = pllxBD;
    }
}
