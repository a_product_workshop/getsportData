package com.bean.skySport;

import java.math.BigDecimal;


//获取到的准备投注数据
public class AllowInvestInfoBase {
	public static final String SHANGBANCHANG="上半场";
	public static final String QUANCHANG="全场";
	public static final String ASIA="让球盘";
	public static final String SIZE="大小盘";
	/** 金额投注 */
	private BigDecimal money;
	/** 最小投注金额 */
	private double minbet;
	/** 最大投注金额 */
	private double maxbet;
	/** 赔率 */
	private double rate;
	/** 主队名称 */
	private String home;
	/** 客队名称 */
	private String away;
	/** 让球盘,大小盘 */
	private String TeamType;
	/** 上半场、全场 */
	private String isFull;
	/** 联赛名称 */
	private String lsName;
	/** 主队赔率类型 */
	private String zpllx;
	/** 客队赔率类型 */
	private String kpllx;
	/** 本次投注的队名 */
	private String investdui;
	/** 主队进球 */
	private int scoreH;
	/** 客队进球 */
	private int scoreA;
	/** 客队得分 */
	public int getScoreA() {
		return scoreA;
	}
	/** 客队得分 */
	public void setScoreA(int scoreA) {
		this.scoreA = scoreA;
	}
	/** 主队得分 */
	public int getScoreH() {
		return scoreH;
	}
	/** 主队得分 */
	public void setScoreH(int scoreH) {
		this.scoreH = scoreH;
	}
	/** 本次投注的队名 */
	public String getInvestdui() {
		return investdui;
	}
	/** 本次投注的队名 */
	public void setInvestdui(String investdui) {
		this.investdui = investdui;
	}
	/** 客队名称 */
	public String getKpllx() {
		return kpllx;
	}
	/** 客队名称 */
	public void setKpllx(String kpllx) {
		this.kpllx = kpllx;
	}
	/** 主队赔率类型 */
	public String getZpllx() {
		return zpllx;
	}
	/** 主队赔率类型 */
	public void setZpllx(String zpllx) {
		this.zpllx = zpllx;
	}
	/** 联赛名称 */
	public String getLsName() {
		return lsName;
	}
	/** 联赛名称 */
	public void setLsName(String lsName) {
		this.lsName = lsName;
	}
	/** 上半场、全场 */
	public String getIsFull() {
		return isFull;
	}
	/** 上半场、全场 */
	public void setIsFull(String isFull) {
		this.isFull = isFull;
	}
	/** 让球盘,大小盘 */
	public String getTeamType() {
		return TeamType;
	}
	/** 让球盘,大小盘 */
	public void setTeamType(String teamType) {
		TeamType = teamType;
	}
	/** 客队名称 */
	public String getAway() {
		return away;
	}
	/** 客队名称 */
	public void setAway(String away) {
		this.away = away;
	}
	/** 主队名称 */
	public String getHome() {
		return home;
	}
	/** 主队名称 */
	public void setHome(String home) {
		this.home = home;
	}
	/** 赔率 */
	public double getRate() {
		return rate;
	}
	/** 赔率 */
	public void setRate(double rate) {
		this.rate = rate;
	}
	/** 最大投注金额 */
	public double getMaxbet() {
		return maxbet;
	}
	/** 最大投注金额 */
	public void setMaxbet(double maxbet) {
		this.maxbet = maxbet;
	}
	/** 最小投注金额 */
	public double getMinbet() {
		return minbet;
	}
	/** 最小投注金额 */
	public void setMinbet(double minbet) {
		this.minbet = minbet;
	}
	/** 金额投注 */
	public BigDecimal getMoney() {
		return money;
	}
	/** 金额投注 */
	public void setMoney(BigDecimal money) {
		this.money = money;
	}

}
