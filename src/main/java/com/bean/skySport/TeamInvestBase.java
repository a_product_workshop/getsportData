package com.bean.skySport;

import java.math.BigDecimal;

/**
 * 球队获取基类（所有的球队获取类都从这个扩展）
 */
public abstract class TeamInvestBase implements Cloneable , java.io.Serializable {
	public static final String SHANGBANCHANG="上半场";
	public static final String QUANCHANG="全场";
	public static final String ASIA="让球盘";
	public static final String SIZE="大小盘";
	/** 上半场、全场 */
	private String isFull;
	/** 联赛名称 */
	private String lsName;
	/** 联赛时间基础 */
	private String lsTimeBase;
	/** 主队名称 */
	private String zdName;
	/** 客队名称 */
	private String kdName;
	/** 让球盘,大小盘 */
	private String teamType;
	/** 主队赔率类型 */
	private String zdPLLX;
	/** 客队赔率类型 */
	private String kdPLLX;
	/** 主队赔率 */
	private BigDecimal zdplBD;
	/** 客队赔率 */
	private BigDecimal kdplBD;
	/** 赔率类型 */
	private BigDecimal pllxBD;
	/** 主队进球 */
	private int scoreH;
	/** 客队进球 */
	private int scoreA;
	/** 第几页 */
	private int webPageIndex=0;
	/** 客队进球 */
	public int getScoreA() {
		return scoreA;
	}
	/** 客队进球 */
	public void setScoreA(int scoreA) {
		this.scoreA = scoreA;
	}
	/** 主队进球 */
	public int getScoreH() {
		return scoreH;
	}
	/** 主队进球 */
	public void setScoreH(int scoreH) {
		this.scoreH = scoreH;
	}
	public boolean equals(Object o){
		TeamInvestBase tib = (TeamInvestBase)o;
		return this.lsName.equals(tib.getLsName()) && this.zdName.equals(tib.getZdName()) && this.kdName.equals(tib.getKdName())  && this.isFull.equals(tib.isFull()) && this.zdPLLX.equals(tib.getZdPLLX()) && this.kdPLLX.equals(tib.getKdPLLX()) && this.teamType.equals(tib.getTeamType());
	}

	public int hashCode(){
		return lsName.hashCode()+zdName.hashCode()+kdName.hashCode()+isFull.hashCode()+zdPLLX.hashCode()+kdPLLX.hashCode()+teamType.hashCode();
	}
	/** 上半场、全场 */
	public String isFull() {
		return isFull;
	}
	/** 上半场、全场 */
	public void setIsFull(String isFull) {
		this.isFull = isFull;
	}
	/** 联赛名称 */
	public String getLsName() {
		return lsName;
	}
	/** 联赛名称 */
	public void setLsName(String lsName) {
		this.lsName = lsName;
	}
	/** 联赛时间基础 */
	public String getLsTimeBase() {
		return lsTimeBase;
	}
	/** 联赛时间基础 */
	public void setLsTimeBase(String lsTimeBase) {
		this.lsTimeBase = lsTimeBase;
	}
	/** 主队名称 */
	public String getZdName() {
		return zdName;
	}
	/** 主队名称 */
	public void setZdName(String zdName) {
		this.zdName = zdName;
	}
	/** 客队名称 */
	public String getKdName() {
		return kdName;
	}
	/** 客队名称 */
	public void setKdName(String kdName) {
		this.kdName = kdName;
	}
	/** 让球盘,大小盘 */
	public String getTeamType() {
		return teamType;
	}
	/** 让球盘,大小盘 */
	public void setTeamType(String teamType) {
		this.teamType = teamType;
	}
	/** 主队赔率类型 */
	public String getZdPLLX() {
		return zdPLLX;
	}
	/** 主队赔率类型 */
	public void setZdPLLX(String zdPLLX) {
		this.zdPLLX = zdPLLX;
	}
	/** 客队赔率类型 */
	public String getKdPLLX() {
		return kdPLLX;
	}
	/** 客队赔率类型 */
	public void setKdPLLX(String kdPLLX) {
		this.kdPLLX = kdPLLX;
	}
	/** 主队赔率 */
	public BigDecimal getZdplBD() {
		return zdplBD;
	}
	/** 主队赔率 */
	public void setZdplBD(BigDecimal zdplBD) {
		this.zdplBD = zdplBD;
	}
	/** 客队赔率 */
	public BigDecimal getKdplBD() {
		return kdplBD;
	}
	/** 客队赔率 */
	public void setKdplBD(BigDecimal kdplBD) {
		this.kdplBD = kdplBD;
	}
	/** 赔率类型 */
	public BigDecimal getPllxBD() {
		return pllxBD;
	}
	/** 赔率类型 */
	public void setPllxBD(BigDecimal pllxBD) {
		this.pllxBD = pllxBD;
	}
	/** 第几页 */
	public int getWebPageIndex() {
		return webPageIndex;
	}
	/** 第几页 */
	public void setWebPageIndex(int webPageIndex) {
		this.webPageIndex = webPageIndex;
	}

}
