package com.bean.skySport;

/**
 *
 */
public class Team extends TeamInvestBase {
    /** 联赛名称 */
    private String lsName;
    /** 联赛时间基础 */
    private String lsTimeBase;
    /** 主队名称 */
    private String zdName;
    /** 客队名称 */
    private String kdName;
    private PK pk;

    @Override
    public String getLsName() {
        return lsName;
    }

    @Override
    public void setLsName(String lsName) {
        this.lsName = lsName;
    }

    @Override
    public String getLsTimeBase() {
        return lsTimeBase;
    }

    @Override
    public void setLsTimeBase(String lsTimeBase) {
        this.lsTimeBase = lsTimeBase;
    }

    @Override
    public String getZdName() {
        return zdName;
    }

    @Override
    public void setZdName(String zdName) {
        this.zdName = zdName;
    }

    @Override
    public String getKdName() {
        return kdName;
    }

    @Override
    public void setKdName(String kdName) {
        this.kdName = kdName;
    }

    public PK getPk() {
        return pk;
    }

    public void setPk(PK pk) {
        this.pk = pk;
    }
}
