package com.bean.skySport;

public class GameInfo {

    /** 联赛名称 */
    private String lsName;
    /** 联赛时间基础 */
    private String lsTimeBase;
    /**
     * 比赛进行实践
     */
    private String matchTime;
    /** 主队名称 */
    private String zdName;
    /** 客队名称 */
    private String kdName;
    /** 主队进球 */
    private int scoreH;
    /** 客队进球 */
    private int scoreA;

    public String getLsName() {
        return lsName;
    }

    public void setLsName(String lsName) {
        this.lsName = lsName;
    }

    public String getLsTimeBase() {
        return lsTimeBase;
    }

    public void setLsTimeBase(String lsTimeBase) {
        this.lsTimeBase = lsTimeBase;
    }

    public String getZdName() {
        return zdName;
    }

    public void setZdName(String zdName) {
        this.zdName = zdName;
    }

    public String getKdName() {
        return kdName;
    }

    public void setKdName(String kdName) {
        this.kdName = kdName;
    }

    public int getScoreH() {
        return scoreH;
    }

    public void setScoreH(int scoreH) {
        this.scoreH = scoreH;
    }

    public int getScoreA() {
        return scoreA;
    }

    public void setScoreA(int scoreA) {
        this.scoreA = scoreA;
    }

    public String getMatchTime() {
        return matchTime;
    }

    public void setMatchTime(String matchTime) {
        this.matchTime = matchTime;
    }
}
