package com.bean;

import java.util.ArrayList;
import java.util.List;

public class ReciveFootballInfo {
	/** 返回的消息,如果有赛事,则msg为空字符串,否则存放返回的提示信息,例如：无足球赛事 */
	private String msg="";
	/** 存放第2个页面的所有赛事数据 */
	private List<FootballInfo> listLianSaiInfo=new ArrayList<FootballInfo>();
	
	/** 返回的消息,如果有赛事,则msg为空字符串,否则存放返回的提示信息,例如：无足球赛事 */
	public String getMsg() {
		return msg;
	}
	/** 返回的消息,如果有赛事,则msg为空字符串,否则存放返回的提示信息,例如：无足球赛事 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
	/** 存放第2个页面的所有赛事数据 */
	public List<FootballInfo> getListLianSaiInfo() {
		return listLianSaiInfo;
	}
	/** 存放第2个页面的所有赛事数据 */
	public void setListLianSaiInfo(List<FootballInfo> listLianSaiInfo) {
		this.listLianSaiInfo = listLianSaiInfo;
	}
	@Override
	public String toString() {
		return "ReciveFootballInfo{" +
				"msg='" + msg + '\'' +
				", listLianSaiInfo=" + listLianSaiInfo +
				'}';
	}
}
