package com.bean;

public class GameStateInfo {
	public static final String EVERGET="数据已经获取过";
	/** 返回的消息,如果有赛事,则msg为空字符串,否则存放返回的提示信息,例如：获取数据异常 数据已经获取过 */
	private String msg="";
	/**主队名*/
	private String zdName;
	/**客队名*/
	private String kdName;
	/** 主队比分 */
	private int homeScoreHome;
	/** 客队比分 */
	private int awayScoreAway;
	/** 进攻队名 */
	private String teamball;
	/** 状态 */
	private String situation;
	/** 主队进攻数 */
	private int homeAttack;
	/** 客队进攻数 */
	private int awayAttack;
	/** 主队危险进攻数 */
	private int homeDangerousAttack;
	/** 客队危险进攻数 */
	private int awayDangerousAttack;
	/** 主队射正球门 */
	private int homeShootGoal;
	/** 客队射正球门 */
	private int awayShootGoal;
	/** 主队射偏球门 */
	private int homeDeviatedGoal;
	/** 客队射偏球门 */
	private int awayDeviatedGoal;
	/** 主队球权 */
	private double homeBallright;
	/** 客队球权 */
	private double awayBallright;
	/** 开赛时间 */
	private String lstime;
	//是否结束比赛
	private  String isMatchEnd;

	
	/** 进攻队名 */
	public String getTeamball() {
		return teamball;
	}
	/** 进攻队名 */
	public void setTeamball(String teamball) {
		this.teamball = teamball;
	}
	/** 状态 */
	public String getSituation() {
		return situation;
	}
	/** 状态 */
	public void setSituation(String situation) {
		this.situation = situation;
	}
	/** 主队进攻数 */
	public int getHomeAttack() {
		return homeAttack;
	}
	/** 主队进攻数 */
	public void setHomeAttack(int homeAttack) {
		this.homeAttack = homeAttack;
	}
	/** 客队进攻数 */
	public int getAwayAttack() {
		return awayAttack;
	}
	/** 客队进攻数 */
	public void setAwayAttack(int awayAttack) {
		this.awayAttack = awayAttack;
	}
	/** 主队危险进攻数 */
	public int getHomeDangerousAttack() {
		return homeDangerousAttack;
	}
	/** 主队危险进攻数 */
	public void setHomeDangerousAttack(int homeDangerousAttack) {
		this.homeDangerousAttack = homeDangerousAttack;
	}
	/** 客队危险进攻数 */
	public int getAwayDangerousAttack() {
		return awayDangerousAttack;
	}
	/** 客队危险进攻数 */
	public void setAwayDangerousAttack(int awayDangerousAttack) {
		this.awayDangerousAttack = awayDangerousAttack;
	}
	/** 主队比分 */
	public int getHomeScoreHome() {
		return homeScoreHome;
	}
	/** 主队比分 */
	public void setHomeScoreHome(int homeScoreHome) {
		this.homeScoreHome = homeScoreHome;
	}
	/** 客队比分 */
	public int getAwayScoreAway() {
		return awayScoreAway;
	}
	/** 客队比分 */
	public void setAwayScoreAway(int awayScoreAway) {
		this.awayScoreAway = awayScoreAway;
	}
	/** 主队射正球门 */
	public int getHomeShootGoal() {
		return homeShootGoal;
	}
	/** 主队射正球门 */
	public void setHomeShootGoal(int homeShootGoal) {
		this.homeShootGoal = homeShootGoal;
	}
	/** 客队射正球门 */
	public int getAwayShootGoal() {
		return awayShootGoal;
	}
	/** 客队射正球门 */
	public void setAwayShootGoal(int awayShootGoal) {
		this.awayShootGoal = awayShootGoal;
	}
	/** 主队射偏球门 */
	public int getHomeDeviatedGoal() {
		return homeDeviatedGoal;
	}
	/** 主队射偏球门 */
	public void setHomeDeviatedGoal(int homeDeviatedGoal) {
		this.homeDeviatedGoal = homeDeviatedGoal;
	}
	/** 客队射偏球门 */
	public int getAwayDeviatedGoal() {
		return awayDeviatedGoal;
	}
	/** 客队射偏球门 */
	public void setAwayDeviatedGoal(int awayDeviatedGoal) {
		this.awayDeviatedGoal = awayDeviatedGoal;
	}
	/** 主队球权 */
	public double getHomeBallright() {
		return homeBallright;
	}
	/** 主队球权 */
	public void setHomeBallright(double homeBallright) {
		this.homeBallright = homeBallright;
	}
	/** 客队球权 */
	public double getAwayBallright() {
		return awayBallright;
	}
	/** 客队球权 */
	public void setAwayBallright(double awayBallright) {
		this.awayBallright = awayBallright;
	}
	/** 返回的消息,如果有赛事,则msg为空字符串,否则存放返回的提示信息,例如：获取数据异常 数据已经获取过 */
	public String getMsg() {
		return msg;
	}
	/** 返回的消息,如果有赛事,则msg为空字符串,否则存放返回的提示信息,例如：获取数据异常 数据已经获取过 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	public static String getEVERGET() {
		return EVERGET;
	}

	public String getLstime() {
		return lstime;
	}

	public void setLstime(String lstime) {
		this.lstime = lstime;
	}

	public String getZdName() {
		return zdName;
	}

	public void setZdName(String zdName) {
		this.zdName = zdName;
	}

	public String getKdName() {
		return kdName;
	}

	public void setKdName(String kdName) {
		this.kdName = kdName;
	}

	public String getIsMatchEnd() {
		return isMatchEnd;
	}

	public void setIsMatchEnd(String isMatchEnd) {
		this.isMatchEnd = isMatchEnd;
	}

	@Override
	public String toString() {
		return "GameStateInfo{" +
				"msg='" + msg + '\'' +
				", zdName='" + zdName + '\'' +
				", kdName='" + kdName + '\'' +
				", homeScoreHome=" + homeScoreHome +
				", awayScoreAway=" + awayScoreAway +
				", teamball='" + teamball + '\'' +
				", situation='" + situation + '\'' +
				", homeAttack=" + homeAttack +
				", awayAttack=" + awayAttack +
				", homeDangerousAttack=" + homeDangerousAttack +
				", awayDangerousAttack=" + awayDangerousAttack +
				", homeShootGoal=" + homeShootGoal +
				", awayShootGoal=" + awayShootGoal +
				", homeDeviatedGoal=" + homeDeviatedGoal +
				", awayDeviatedGoal=" + awayDeviatedGoal +
				", homeBallright=" + homeBallright +
				", awayBallright=" + awayBallright +
				", lstime='" + lstime + '\'' +
				", isMatchEnd='" + isMatchEnd + '\'' +
				'}';
	}
}
