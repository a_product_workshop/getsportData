package com.bean;

import org.openqa.selenium.WebElement;

public class FootballInfo {
	/** 联赛名 */
	private String lsname;
	/** 主队名 */
	private String zdname;
	/** 客队名 */
	private String kdname;
	/** 开赛时间 */
	private String lstime;
	//查看当前场次比赛按钮
	private  String seeSingleMatchButtonXpath;
	private String msg;
	/** 联赛名 */
	public String getLsname() {
		return lsname;
	}
	/** 联赛名 */
	public void setLsname(String lsname) {
		this.lsname = lsname;
	}
	/** 主队名 */
	public String getZdname() {
		return zdname;
	}
	/** 主队名 */
	public void setZdname(String zdname) {
		this.zdname = zdname;
	}
	/** 客队名 */
	public String getKdname() {
		return kdname;
	}
	/** 客队名 */
	public void setKdname(String kdname) {
		this.kdname = kdname;
	}
	/** 开赛时间 */
	public String getLstime() {
		return lstime;
	}
	/** 开赛时间 */
	public void setLstime(String lstime) {
		this.lstime = lstime;
	}

	public String getSeeSingleMatchButtonXpath() {
		return seeSingleMatchButtonXpath;
	}

	public void setSeeSingleMatchButtonXpath(String seeSingleMatchButtonXpath) {
		this.seeSingleMatchButtonXpath = seeSingleMatchButtonXpath;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	@Override
	public String toString() {
		return "FootballInfo{" +
				"lsname='" + lsname + '\'' +
				", zdname='" + zdname + '\'' +
				", kdname='" + kdname + '\'' +
				", lstime='" + lstime + '\'' +
				", seeSingleMatchButtonXpath='" + seeSingleMatchButtonXpath + '\'' +
				'}';
	}
}
