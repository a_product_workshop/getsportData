package com.locators;

/**
 * Created by zhengshuheng on 2017/3/15 0015.
 */
public class Locators {
    public static final  String SELECTLANGUAGE="//a[contains(text(),'简体中文')]";
    public static final String  GQP="//a[contains(@class,'hm-BigButton') and text()='滚球盘']";
    public static final String SEEPK="//div[contains(@class,'ip-ControlBar_BBarItem wl-ButtonBar_Selected') and text()='盘口查看']";
    public static final String SPORTNAME="//div[contains(@class,'ipo-ClassificationBarButtonBase_Label') and text()='足球']";
    public static final String MatchLive="//div[text()='比赛现场']";
}
