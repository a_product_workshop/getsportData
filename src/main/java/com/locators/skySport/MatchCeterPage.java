package com.locators.skySport;


/**
 * Created by zhengshuheng on 2017/9/23.
 */
public class MatchCeterPage {
    public static final String matchCenterMainFrame="//*[@id='spshowFrame']";
    public static final String matchCenterHeaderFrame="//*[@id='header']";
    public static final String matchCenterContentFrame="//*[@id='showFrame']";
    public static final String footBallMenu=".//*[@id='navli4']/span/a[contains(text(),'足球')]";
    public static final String lastPageIndex=".//*[@id='pg_txt']//a[@title='最后页']";
    public static final  String pageNow=".//*[@id='pg_txt']//*[contains(@class,'pageNow')]";
    public static final String gunqiu=".//*[@id='rbyshow']";
    public static String getPageNumXpath(int page){
        String pageNumXpath=".//*[@id='pg_txt']//*[contains(text(),'[ "+page+" ]')]";
        return pageNumXpath;
    }
    public static final String lsNames=".//*[@id='data']/tbody//td[@class='b_title']";
    public static final String lsTimes=".//*[@id='data']/tbody/tr/td[1]";
    public static final String zkdNames=".//*[@id='data']/tbody/tr/td[@rowspan='2']";//主客队名xpath
    public static final String bfs=".//*[@id='data']/tbody/tr/td[1]/div[@class='bf']";

    /**
     * 获取第几场联赛名称xpath
     * @param index
     * @return
     */
    public static final String getLsElementXpth(int index){
        String lsElementXpath="("+MatchCeterPage.lsNames+")["+index+"]";
        return lsElementXpath;
    }
    public static final String getFirstBfXpath(String lsElementXpath){
        String firstBfXpath=lsElementXpath+"/../following-sibling::tr[1]/td[1]/div[@class='bf']";
        return firstBfXpath;
    }
    public static final String getnextMatchXpath(String nextBfElementXpath){
        String nextMatchXpath=nextBfElementXpath+"/../../following-sibling::tr[4]";
        return nextMatchXpath;
    }
    public static final String getNextBfXpath(String nextMatchXpath ){
        String nextBfXpath=nextMatchXpath+"/td[1]/div[@class='bf']";
        return nextBfXpath;
    }
    public static final String getZkdNameXpath(String bfElementXpath){
        return bfElementXpath+"/../following-sibling::td[1]";
    }

    public static final String selectLs=".//*[@id='sportwrapbox']//a[contains(text(),'选择联赛')]";
    public static String selectALsXpath(String lsName){
        String selectLsXpath=".//*[@id='leagueForm']//input[@value='"+lsName+"']";
        return selectLsXpath;
    }
    public static final String  allNoSelectLsButton=".//*[@id='leagueForm']/div/input[@value='全不选']";
    public static final String lsConfirmButton=".//*[@id='leagueForm']/div/input[@value='确定']";




}
