package com.locators.skySport;

/**
 * Created by zhengshuheng on 2017/9/22.
 */
public class HomePage {
    public  static  final String userName=".//*[@id='username']";
    public  static  final String password1=".//*[@id='txtFacade']";
    public  static  final String password2=".//*[@id='txtPasword']";
    public static final String login=".//*[@name='Submit']";
    public static final String webcomePageCloseButon="//*[text()='关闭']";
    public static final String hg_sport=".//*[@id='container']//a[@href='/hg_sports']";

}
