package com.locators;

public class GameStateInfoLocators {
	/** 主队比分 */
	public static final String homeScoreHome="//div[contains(@class,'ml1-ScoreHeader_Column ml1-ScoreHeader_Column-left')]//div[contains(@class,'ml1-ScoreHeader_ScoreContainer')]//div";//m178
	/** 客队比分 */
	public static final String awayScoreAway="//div[contains(@class,'ml1-ScoreHeader_Column ml1-ScoreHeader_Column-right')]//div[contains(@class,'ml1-ScoreHeader_ScoreContainer')]//div";
	/** 进攻队名 */
	public static final String teamball="(//span[contains(@class,'ml1-AnimatedTextBar ml1-CommonAnimations_H1Text')])[1]";
	/** 状态 */
	public static final String situation="//div[contains(@class,'ml1-AnimatedTextBar ml1-CommonAnimations_Asset ml1-CommonAnimations_H2TextWrapper')]//span[contains(@class,'ml1-AnimatedTextBar ml1-CommonAnimations_H2Text')]";
	/** 主队进攻数 */
	public static final String homeAttack="(//div[contains(@class,'ml1-StatWheel_Team1Text')])[1]";
	/** 客队进攻数 */
	public static final String awayAttack="(//div[contains(@class,'ml1-StatWheel_Team2Text')])[1]";
	/** 主队危险进攻数 */
	public static final String homeDangerousAttack="(//div[contains(@class,'ml1-StatWheel_Team1Text')])[2]";;
	/** 客队危险进攻数 */
	public static final String awayDangerousAttack="(//div[contains(@class,'ml1-StatWheel_Team2Text')])[2]";
	/** 主队射正球门 */
	public static final String homeShootGoal="(//span[contains(@class,'ml1-SoccerStatsBar_MiniBarValue ml1-SoccerStatsBar_MiniBarValue-1')])[1]";
	/** 客队射正球门 */
	public static final String awayShootGoal="(//span[contains(@class,'ml1-SoccerStatsBar_MiniBarValue ml1-SoccerStatsBar_MiniBarValue-2')])[1]";
	/** 主队射偏球门 */
	public static final String homeDeviatedGoal="(//span[contains(@class,'ml1-SoccerStatsBar_MiniBarValue ml1-SoccerStatsBar_MiniBarValue-1')])[2]";
	/** 客队射偏球门 */
	public static final String awayDeviatedGoal="(//span[contains(@class,'ml1-SoccerStatsBar_MiniBarValue ml1-SoccerStatsBar_MiniBarValue-2')])[1]";;
	/** 主队球权 */
	public static final String  homeBallright="(//div[contains(@class,'ml1-StatWheel_Team1Text')])[3]";
	/** 客队球权 */
	public static final String awayBallright="(//div[contains(@class,'ml1-StatWheel_Team2Text')])[3]";
	/**比赛开始时间**/
	public static final  String lsTime="//span[contains(@class,'ml1-ScoreHeader_Clock')]";
	/**检查比赛是否半场**/
	public static final  String half="//div[contains(@class,'ml1-AnimatedTextBar ml1-CommonAnimations_Asset ml1-CommonAnimations_H2TextWrapper')]//span[contains(@class,'ml1-AnimatedTextBar ml1-CommonAnimations_H2Text') and text()='半场']";//等定位2个标签
	/**检查比赛是否结束**/
	public static final String isMachEnd="//div[contains(@class,'ml1-FormLink_FormLink') and text()='状态'] ";
	public static final  String zdName="//div[contains(@class,'ml1-ScoreHeader_Column ml1-ScoreHeader_Column-left ')]//div[contains(@class,'ml1-ScoreHeader_TeamText')]";
	public static final String kdName="//div[contains(@class,'ml1-ScoreHeader_Column ml1-ScoreHeader_Column-right ')]//div[contains(@class,'ml1-ScoreHeader_TeamText')]";
	//全场
	public static final String fullCourt="//span[contains(@class,'ml1-AnimatedTextBar ml1-CommonAnimations_H2Text') and text()='全场']";

}
