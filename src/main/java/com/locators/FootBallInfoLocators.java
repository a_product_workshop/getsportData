package com.locators;

/**
 * Created by Administrator on 2017/3/15 0015.
 */
public class FootBallInfoLocators {
    /** 球队名(含主队和客队) */
    public static final String qdname="//span[@class='ipo-TeamStack_TeamWrapper']";
    public static  final String zdName="(//span[@class='ipo-TeamStack_TeamWrapper'])[1]";
    public static  final String kdName="(//span[@class='ipo-TeamStack_TeamWrapper'])[2]";
    /** 开赛时间 */
    public static final  String lstime="//div[contains(@class,'ipo-InPlayTimer')]";
    /**联赛容器*/
    public static  final String lsContainer="//div[contains(@class,'ipo-Competition ipo-Competition-open')]";
    /**场次容器*/
    public static  final String ccContainer="//div[contains(@class,'ipo-Fixture_ScoreDisplay ipo-ScoreDisplayStandard')]";
    /**场次信息查看按钮**/
    public static  final String seeMatchInfo="//div[contains(@class,'wl-MediaButtonLoader wl-MediaButtonLoader_ML1')]";
    /**比赛现场容器**/
    public static final String matchLiveContainer="//div[contains(@class,'lv-MatchLiveView_Container')]";
    public static final  String lsName="//div[contains(@class,'ipo-CompetitionButton_NameLabel ipo-CompetitionButton_NameLabelHasMarketHeading')]";
}
