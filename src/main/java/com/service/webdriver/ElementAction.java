package com.service.webdriver;

import com.service.webdriver.Browser;
import com.utils.Log;
import com.utils.ScreenShot;
import com.utils.TimeUtil;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/3/15.
 */
public class ElementAction  extends Browser {
    Log log=new Log(this.getClass());
    /**
     *
     * @param
     */
    public void sleep(long milliSecond)
    {
        try {
            log.info("等待"+milliSecond+"豪秒");
            Thread.sleep(milliSecond);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    /**
     *
     * @param
     */
    public void sleep(long milliSecond,String message)
    {
        try {
            log.info(message+" 等待"+milliSecond+"豪秒");
            Thread.sleep(milliSecond);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public List<WebElement> findElements(String xpath){
        List<WebElement> webElementList=driver.findElements(By.xpath(xpath));
        log.info("查找一组元素"+xpath+"共找到"+webElementList.size()+"个元素");
        return webElementList;
    }
    /**
     * 查找元素并点击
     * @param
     * @param xpath
     * @return
     */
    public WebElement findElementAndClick(String xpath) {
        WebElement webElement=findElement(xpath,15);
        log.info("点击元素"+xpath);
        webElement.click();
        return webElement;
    }
    /**
     * 查找元素并点击
     * @param
     * @param xpath
     * @return
     */
    public WebElement findElementAndClick(String xpath,Integer timeOut) {
        WebElement webElement=findElement(xpath,timeOut);
        log.info("点击元素"+xpath);
        webElement.click();
        return webElement;
    }

    /**
     * 获取文本
     * @param xpath
     * @return
     */
    public String getText(String xpath){
        String text="";
        try {
            WebElement webElement=findElement(xpath,4);
            text=webElement.getText();
            log.debug("获取到文本"+text);
        }catch (TimeoutException e){
//            e.printStackTrace();
            log.debug("不存在此元素"+xpath);
            text="-1";//无数据置为-1
            log.debug("获取到文本"+text);

        }
        return  text;
    }
    /**
     * 获取文本
     * @param xpath
     * @return
     */
    public String getText(WebElement parentElement,String xpath){
        String text="";
        try {
            WebElement webElement=findElement(parentElement,xpath,4);
            text=webElement.getText();
            log.debug("获取到文本"+text);
        }catch (TimeoutException e){
//            e.printStackTrace();
            log.debug("不存在此元素"+xpath);
            text="-1";//无数据置为-1
            log.debug("获取到文本"+text);

        }
        return  text;
    }

    /**
     * 获取文本
     * @param xpath
     * @return
     */
    public String getText(String xpath,long timeout){
        String text="";
        try {
            WebElement webElement=findElement(xpath,timeout);
            text=webElement.getText();
            log.debug("获取到文本"+text);
        }catch (Exception e){
            //e.printStackTrace();
            ScreenShot screenShot=new ScreenShot(driver);
            Date nowDate=new Date();
            log.debug("不存在此元素"+xpath);
            text="-1";//无数据置为-1
//            log.info("获取到文本"+text);
            String snapshotName=TimeUtil.formatDate(nowDate,"yyyy-MM-dd HHmmssSSS");
            screenShot.setscreenName(snapshotName);
            screenShot.takeScreenshot();

        }

        return  text;
    }

    /**
     * 获取文本
     * @param xpath
     * @return
     */
    public String getText(WebElement parentElement,String xpath,long timeOut){
        String text="";
        try {
            WebElement webElement=findElement(parentElement,xpath,timeOut);
            text=webElement.getText();
            log.debug("获取到文本"+text);
        }catch (Exception e){
//            e.printStackTrace();
            log.debug("不存在此元素"+xpath);
            text="-1";//无数据置为-1
            log.debug("获取到文本"+text);

        }
        return  text;
    }
    public WebElement findElement(String xpath,long timeout){
        /**
         * 查找某个元素等待几秒
         */
        WebElement webElement=null;
        webElement=(new WebDriverWait(driver,timeout)).until(
                new ExpectedCondition<WebElement>() {
                    @Override
                    public WebElement apply(WebDriver driver) {
                        log.debug("查找元素"+xpath);
                        WebElement element =driver.findElement(By.xpath(xpath));
                        return element;
                    }
                });
        return webElement;
    }

    public WebElement findElement(WebElement parentElement,String xpath,long timeout){
        /**
         * 查找某个元素等待几秒
         */
        WebElement webElement=null;
        webElement=(new WebDriverWait(driver,timeout)).until(
                new ExpectedCondition<WebElement>() {
                    @Override
                    public WebElement apply(WebDriver driver) {
                        log.debug("查找元素"+xpath);
                        WebElement element =parentElement.findElement(By.xpath(xpath));
                        return element;
                    }
                });
        return webElement;
    }
    public WebElement findElement(String xpath){
        /**
         * 查找某个元素等待几秒
         */
        WebElement webElement=null;
        webElement=(new WebDriverWait(driver,15)).until(
                new ExpectedCondition<WebElement>() {
                    @Override
                    public WebElement apply(WebDriver driver) {
                        log.debug("查找元素"+xpath);
                        WebElement element =driver.findElement(By.xpath(xpath));
                        return element;
                    }
                });
        return webElement;
    }
    /**
     * 显式等待 判断页面是否完全加载完成
     * @param time 已秒为单位
     */
    public void pagefoload(WebDriver driver, long time)
    {
        log.info("智能等待"+time+"秒加载使页面加载完成");
        ExpectedCondition<Boolean> pageLoad= new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        WebDriverWait wait = new WebDriverWait(driver, time);
        wait.until(pageLoad);
    }
    public void openNewTab(Integer i){
         executeJS("window.open()");
        log.info("第"+i+"个线程切换窗口");
    }
    public void executeJS(String js) {
        try {
            ((JavascriptExecutor) driver).executeScript(js);
            log.info("执行js脚本"+js+"成功");
        }catch (Exception e){
            log.info("执行js脚本"+js+"失败");
        }

    }
    public boolean isExistWebMent(String xpath,long timeout){
        boolean flag=false;
        try {
            findElement(xpath,timeout);
            flag=true;
        }catch (Exception e){
            //e.printStackTrace();
            flag=false;
        }
        return flag;
    }
}
