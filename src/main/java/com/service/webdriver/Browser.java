package com.service.webdriver;

import com.utils.Log;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by Administrator on 2017/3/15.
 */
public class Browser {
    Log log=new Log(this.getClass());
    public static ThreadLocal<WebDriver> webDriver=new ThreadLocal<WebDriver>();
    public static WebDriver driver=webDriver.get();

    public  void setWebDriver()
    {
        //driver=getWebDriver("chrome");
      driver=getRemoteWebDriver("chrome","http://127.0.0.1:3166/wd/hub/");
    }
    /**
     * 获取浏览器对象--本地运行模式不支持，并发
     * @param browserName
     * @return
     */
    private  WebDriver getWebDriver(String browserName){
        WebDriver driver2;
        switch (browserName){
            case "chrome":
                System.setProperty("webdriver.chrome.driver", "D:\\auto\\webdriver\\chromedriver.exe");
                driver2=new ChromeDriver();
                driver2.manage().window().maximize();
                driver2.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
                driver2.manage().deleteAllCookies();
                webDriver.set(driver2);
                break;
            case "firefox":
                driver2=new FirefoxDriver();
                driver2.manage().window().maximize();
                driver2.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
                driver2.manage().deleteAllCookies();
                webDriver.set(driver2);
                break;
        }
        return webDriver.get();
    }
    /**
     * 获取浏览器对象--remoteDriver运行模式支持并发
     * @param browserName
     * @return
     */
    public   WebDriver getRemoteWebDriver(String browserName,String nodeURL)  {
        WebDriver driver2=null;
        switch (browserName){
            case "chrome":
                DesiredCapabilities dcchorme=DesiredCapabilities.chrome();
                ChromeOptions options = new ChromeOptions();
                dcchorme.setBrowserName("chrome");
                //dcchorme.setVersion("56.0.2924.87");
                dcchorme.setPlatform(Platform.ANY);
                dcchorme.setCapability(ChromeOptions.CAPABILITY,options);
                try {
                    driver2=new RemoteWebDriver(new URL(nodeURL), dcchorme);
                } catch (MalformedURLException e) {
                    log.info("selenium grid url不对");
                    e.printStackTrace();
                }
                driver2.manage().timeouts().implicitlyWait(1,TimeUnit.SECONDS);
                driver2.manage().window().maximize();
                driver2.manage().timeouts().implicitlyWait(1000, TimeUnit.MILLISECONDS);
                driver2.manage().deleteAllCookies();
                webDriver.set(driver2);
                break;
            case "firefox":
                DesiredCapabilities capabilities=DesiredCapabilities.firefox();
                capabilities.setBrowserName("firefox");
                capabilities.setPlatform(Platform.WINDOWS);
                try {
                    driver2=new RemoteWebDriver(new URL(nodeURL), capabilities);
                } catch (MalformedURLException e) {
                    log.info("selenium grid url不对");
                    e.printStackTrace();
                }
                webDriver.set(driver2);
                break;
        }
        return webDriver.get();
    }
}
