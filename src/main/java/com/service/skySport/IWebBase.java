package com.service.skySport;

import com.bean.skySport.AllowInvestInfoBase;
import com.bean.skySport.TeamInvest;
import com.bean.skySport.TeamInvestBase;

import java.util.List;

public interface IWebBase {
	/**
	 * 登录
	 * @return 成功\密码错误...
	 */
	public  String login(String userName,String password);
	/**
	 * 获取赛事的总页数
	 * @return 总页数
	 */
	public  int getSumPage();
	/**
	 * 获取某页的赛事数据，注：要获取赛事数据的DIV节点的html后再分析赛事
	 * @param page 第几页
	 * @return 该页的赛事数据
	 */
	//public  List<TeamInvestBase> getSaiShi(int page);
	public  List<TeamInvest> getSaiShi(int page);
	/**
	 * 点击某个赛事盘口。注：要获取点击盘口的DIV节点的html后再分析赛事
	 * @param teamInvestBase 赛事盘口
	 * @return 点击后的赛事盘口数据
	 */
	public AllowInvestInfoBase queryInvestInfo(TeamInvestBase teamInvestBase);
	/**
	 * 投注
	 * @param allowInvestInfoBase 要投注的赛事盘口信息
	 * @return 投注成功/失败。。。
	 */
	public  String InvestOperate(AllowInvestInfoBase allowInvestInfoBase);
	/**
	 * 返回投注历史数据。注：要获取历史数据的DIV节点的html后再分析赛事
	 * @return 投注后的历史数据
	 */
	public  List<AllowInvestInfoBase> investResult();
}
