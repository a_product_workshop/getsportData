package com.service.action;

import com.locators.Locators;
import com.service.webdriver.Browser;
import com.service.webdriver.ElementAction;
import com.utils.Log;
import org.openqa.selenium.*;

import java.util.ArrayList;

/**
 * Created by Administrator on 2017/3/15.
 */
public class EnterFootBallHome extends Browser {
    Log log=new Log(this.getClass());
    ElementAction action=new ElementAction();
    /**
     * 进入足球主页
     * @param
     * @return
     */
    public WebDriver enterHome()
    {
       driver.get("https://www.bet365.com/#/IP/");
        action.pagefoload(driver,5);
        ClickService clickService=new ClickService();
        try {
            //选择语言
            clickService.clickSelectLanguage();
            action.pagefoload(driver,5);
        } catch (Exception e) {
            log.info("有缓存无需点击语言切换菜单");
        }
        action.pagefoload(driver,5);//切换新窗口后等待页面加载完成再进行数据查找
        //点击滚球盘
        clickService.clickGQP();
        action.sleep(500);
        try {
            //点击盘口查看
            clickService.clickPk();
        }catch (Exception e){
            log.info("点击盘口查看菜单，10秒超时失败，失败重试1次");
            //点击盘口查看
            clickService.clickPk();
        }
        CheckLsStateService checkLsStateService=new CheckLsStateService();
        if (!checkLsStateService.checkLsIsOver()){//假如有足球比赛才去点击足球菜单，没有则不点击
            //点击足球菜单
            clickService.clickSportName();
            action.sleep(1);
        }
        return  driver;
    }
    /**
     * 进入足球主页-新标签模式；新开标签，无需选择语言
     * @param
     * @return
     */
    public WebDriver enterHomeForNewTab()
    {
        driver.get("https://www.bet365.com/#/IP/");
        ClickService clickService=new ClickService();
        action.pagefoload(driver,5);//切换新窗口后等待页面加载完成再进行数据查找
        //点击滚球盘
        try {
            clickService.clickGQP();
            action.sleep(500);
        }catch (StaleElementReferenceException e ){
            clickService.clickGQP();
        }
        try {
            //点击盘口查看
            clickService.clickPk();
        }catch (TimeoutException e){
            log.info("点击盘口查看菜单，10秒超时失败，失败重试1次");
            //点击盘口查看
            clickService.clickPk();

        }
        CheckLsStateService checkLsStateService=new CheckLsStateService();
        if (!checkLsStateService.checkLsIsOver()){//假如有足球比赛才去点击足球菜单，没有则不点击
            //点击足球菜单
            clickService.clickSportName();
            action.sleep(1);
        }
        return  driver;
    }
}
