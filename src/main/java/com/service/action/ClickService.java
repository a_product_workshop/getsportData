package com.service.action;

import com.locators.FootBallInfoLocators;
import com.locators.Locators;
import com.service.webdriver.ElementAction;
import org.openqa.selenium.WebElement;

/**
 * Created by Administrator on 2017/3/15.
 */
public class ClickService {
    ElementAction action=new ElementAction();
    //点击比赛现场
    public void clickMatchLive(){
        action.findElementAndClick(Locators.MatchLive);//点击比赛现场
        action.sleep(2000,"点击比赛现场");
    }

    //点击盘口
    public void clickPk(){
        //点击盘口查看
        action.findElementAndClick(Locators.SEEPK);
        action.sleep(3000,"点击盘口查看菜单");
    }
    //点击选择语言
    public void clickSelectLanguage(){
        action.findElementAndClick(Locators.SELECTLANGUAGE);
        action.sleep(2000,"点击选择语言菜单");
    }
    //点击足球菜单
    public void clickSportName(){
        //点击足球菜单
        action.findElementAndClick(Locators.SPORTNAME);
        action.sleep(3000,"点击足球菜单");

    }
    //点击滚球盘
    public void clickGQP()
    {
        action.findElementAndClick(Locators.GQP);
        action.sleep(3000,"点击滚球盘");
    }
}
