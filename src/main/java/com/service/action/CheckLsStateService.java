package com.service.action;

import com.locators.FootBallInfoLocators;
import com.locators.GameStateInfoLocators;
import com.locators.Locators;
import com.service.webdriver.ElementAction;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;

/**
 * Created by Administrator on 2017/3/15.
 */
public class CheckLsStateService {
    ElementAction action=new ElementAction();
    /**
     * 检查是否有足球联赛;假如找不到足球菜单则说明足球赛事已停止;联赛没结束返回false，已结束返回true
     * @return
     */
    public boolean checkLsIsOver(){
        boolean flag=false;
        if (action.findElements(Locators.SPORTNAME).size()>0){
            flag=false;
        }else {
            flag=true;
        }
        return flag;
    }

    /**
     * 判断比赛是否是半场状态;如果是半场状态返回true,不是半场返回false
     * @return
     */
    public boolean checkLsIsHalf(){
        boolean flag=false;
        if (action.findElements(GameStateInfoLocators.half).size()>0){
            flag=true;
        }else {
            flag=false;
        }

        return flag;
    }

    /**
     * 检查当前比赛是否结束
     * @return
     */
    public boolean checkIsEndMatch(){
        boolean flag=false;
        if (action.findElements(GameStateInfoLocators.isMachEnd).size()>0){
            flag=true;
        }else {
            flag=false;
        }

        return flag;
    }
    /**
     * 检查当前比赛是否全场开始状态
     * @return
     */
    public boolean checkIsFullCourt(){
        boolean flag=false;
        if (action.findElements(GameStateInfoLocators.fullCourt).size()>0){
            flag=true;
        }else {
            flag=false;
        }

        return flag;
    }
}
