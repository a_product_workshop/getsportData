package com.service.getdata;

import com.bean.FootballInfo;
import com.bean.ReciveFootballInfo;
import com.service.action.CheckLsStateService;
import com.service.webdriver.Browser;

import java.util.List;

/**
 * Created by Administrator on 2017/3/15.
 */
public class ReciveFootBallInfoService extends Browser {
    FootballInfoService footballInfoService=new FootballInfoService();
    GameStateInfoService gameStateInfoService=new GameStateInfoService();
    CheckLsStateService checkLsStateService=new CheckLsStateService();

    public ReciveFootballInfo getReciveFootBallInfo()
    {
        ReciveFootballInfo reciveFootballInfo=new ReciveFootballInfo();
        if (!checkLsStateService.checkLsIsOver()){//假如有足球联赛
            List<FootballInfo> footballInfos=footballInfoService.getAllFootBallInfo();
            reciveFootballInfo.setListLianSaiInfo(footballInfos);
            reciveFootballInfo.setMsg("");
        }else {//联赛结束
            reciveFootballInfo.setMsg("无足球赛事");
        }
        return reciveFootballInfo;
    }
    public static void main(String[] args){

    }
}
