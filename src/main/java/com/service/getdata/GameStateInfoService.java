package com.service.getdata;

import com.bean.FootballInfo;
import com.bean.GameStateInfo;
import com.locators.GameStateInfoLocators;
import com.locators.Locators;
import com.service.action.CheckLsStateService;
import com.service.webdriver.Browser;
import com.service.webdriver.ElementAction;
import com.utils.Log;
import com.utils.ScreenShot;
import com.utils.TimeUtil;
import org.openqa.selenium.StaleElementReferenceException;

import java.util.Date;

/**
 * Created by zhengshuheng on 2017/3/15.
 */
public class GameStateInfoService extends Browser {
    Log log=new Log(this.getClass());
    ElementAction action=new ElementAction();


    /**
     * 通过 联赛名，主队名，客队名获取比赛现场信息
     * @param footballInfo
     * @return
     */
    public GameStateInfo getSingleGameStateInfo(FootballInfo footballInfo){
        CheckLsStateService checkLsStateService=new CheckLsStateService();
        GameStateInfo gameStateInfo=new GameStateInfo();
        if (!checkLsStateService.checkLsIsOver()){
            log.info("点击查看当前赛事按钮");
//            action.findElement(footballInfo.getSeeSingleMatchButtonXpath(),5).click();
            try {
                action.findElementAndClick(footballInfo.getSeeSingleMatchButtonXpath());
                action.sleep(3000);
            }catch (Exception e){
                ScreenShot screenShot=new ScreenShot(driver);
                Date nowDate=new Date();
                String snapshotName=TimeUtil.formatDate(nowDate,"yyyy-MM-dd HHmmssSSS");
                screenShot.setscreenName(snapshotName);
                screenShot.takeScreenshot();
            }

           gameStateInfo =getGameStateInfo();
        }else {
            gameStateInfo.setMsg("当前无比赛信息");
        }
        return gameStateInfo;
    }
    public GameStateInfo getGameStateInfo(){
        CheckLsStateService checkLsStateService=new CheckLsStateService();
        GameStateInfo gameStateInfo=new GameStateInfo();
        try {
            if (!checkLsStateService.checkLsIsHalf()&&!checkLsStateService.checkIsEndMatch()&&!checkLsStateService.checkIsFullCourt()){//检查是否半场,是否结束比赛状态,是否全场状态
                action.sleep(500);
                gameStateInfo=getGameStateInfoFromOnline();
            }else if (checkLsStateService.checkIsEndMatch()){//检查是否结束比赛
                action.sleep(500);
                gameStateInfo=getGameStateInfoFromOnlineToEndMatch();
            }else if (checkLsStateService.checkIsFullCourt()){
                action.sleep(500);
                gameStateInfo.setMsg("全场状态");
            }else {
                action.sleep(500);
                gameStateInfo.setMsg("半场状态");//半场状态
            }
           return gameStateInfo;
        }catch (StaleElementReferenceException e){//如果出现此异常则重试
            return getGameStateInfo();
        }catch (NumberFormatException e){
            ScreenShot screenShot=new ScreenShot(driver);
            long time=System.currentTimeMillis();
            screenShot.setscreenName(TimeUtil.formatDate(time)+".jpg");
            gameStateInfo.setMsg("获取数据失败");//半场状态
            screenShot.takeScreenshot();
            return gameStateInfo;
        }
    }
    /**
     * 在线获取比赛现场数据
     * @return
     */
    private GameStateInfo getGameStateInfoFromOnline()
    {
             GameStateInfo gameStateInfo=new GameStateInfo();
            //抓取比赛现场信息
            /** 主队比分 */
            int homeScoreHome=Integer.valueOf(action.getText(GameStateInfoLocators.homeScoreHome));
            /** 客队比分 */
            int awayScoreAway=Integer.valueOf(action.getText(GameStateInfoLocators.awayScoreAway));
            /** 进攻队名 */
            String teamball=action.getText(GameStateInfoLocators.teamball);
            /** 状态 */
            String situation=action.getText(GameStateInfoLocators.situation);
            /** 主队进攻数 */
            int homeAttack=Integer.valueOf(action.getText(GameStateInfoLocators.homeAttack));
            /** 客队进攻数 */
            int awayAttack=Integer.valueOf(action.getText(GameStateInfoLocators.awayAttack));
            /** 主队危险进攻数 */
            int homeDangerousAttack=Integer.valueOf(action.getText(GameStateInfoLocators.homeDangerousAttack));
            /** 客队危险进攻数 */
            int awayDangerousAttack=Integer.valueOf(action.getText(GameStateInfoLocators.awayDangerousAttack));
            /** 主队射正球门 */
            int homeShootGoal=Integer.valueOf(action.getText(GameStateInfoLocators.homeShootGoal));
            /** 客队射正球门 */
            int awayShootGoal=Integer.valueOf(action.getText(GameStateInfoLocators.awayShootGoal));
            /** 主队射偏球门 */
            int homeDeviatedGoal=Integer.valueOf(action.getText(GameStateInfoLocators.homeDeviatedGoal));
            /** 客队射偏球门 */
            int awayDeviatedGoal=Integer.valueOf(action.getText(GameStateInfoLocators.awayDeviatedGoal));
            /** 主队球权 */
            double homeBallright=Double.valueOf(action.getText(GameStateInfoLocators.homeBallright));
            /** 客队球权 */
            double awayBallright=Double.valueOf(action.getText(GameStateInfoLocators.awayBallright));
           /** 开赛时间 */
            String lstime=action.getText(GameStateInfoLocators.lsTime);
            /** 主队名*/
            String zdName=action.getText(GameStateInfoLocators.zdName);
            /**客队名*/
            String kdName=action.getText(GameStateInfoLocators.kdName);
            gameStateInfo.setHomeScoreHome(homeScoreHome);
            gameStateInfo.setAwayScoreAway(awayScoreAway);
            gameStateInfo.setTeamball(teamball);
            gameStateInfo.setSituation(situation);
            gameStateInfo.setHomeAttack(homeAttack);
            gameStateInfo.setAwayAttack(awayAttack);
            gameStateInfo.setHomeDangerousAttack(homeDangerousAttack);
            gameStateInfo.setAwayDangerousAttack(awayDangerousAttack);
            gameStateInfo.setHomeShootGoal(homeShootGoal);
            gameStateInfo.setAwayShootGoal(awayShootGoal);
            gameStateInfo.setHomeDeviatedGoal(homeDeviatedGoal);
            gameStateInfo.setAwayDeviatedGoal(awayDeviatedGoal);
            gameStateInfo.setHomeBallright(homeBallright);
            gameStateInfo.setAwayBallright(awayBallright);
            gameStateInfo.setLstime(lstime);
            gameStateInfo.setZdName(zdName);
            gameStateInfo.setKdName(kdName);

        return gameStateInfo;
    }
    private GameStateInfo getGameStateInfoFromOnlineToEndMatch(){
        //抓取比赛现场信息
        GameStateInfo gameStateInfo=new GameStateInfo();
        /** 主队比分 */
        int homeScoreHome=Integer.valueOf(action.getText(GameStateInfoLocators.homeScoreHome));
        /** 客队比分 */
        int awayScoreAway=Integer.valueOf(action.getText(GameStateInfoLocators.awayScoreAway));
        /** 主队进攻数 */
        int homeAttack=Integer.valueOf(action.getText(GameStateInfoLocators.homeAttack));
        /** 客队进攻数 */
        int awayAttack=Integer.valueOf(action.getText(GameStateInfoLocators.awayAttack));
        /** 主队危险进攻数 */
        int homeDangerousAttack=Integer.valueOf(action.getText(GameStateInfoLocators.homeDangerousAttack));
        /** 客队危险进攻数 */
        int awayDangerousAttack=Integer.valueOf(action.getText(GameStateInfoLocators.awayDangerousAttack));
        /** 主队射正球门 */
        int homeShootGoal=Integer.valueOf(action.getText(GameStateInfoLocators.homeShootGoal));
        /** 客队射正球门 */
        int awayShootGoal=Integer.valueOf(action.getText(GameStateInfoLocators.awayShootGoal));
        /** 主队射偏球门 */
        int homeDeviatedGoal=Integer.valueOf(action.getText(GameStateInfoLocators.homeDeviatedGoal));
        /** 客队射偏球门 */
        int awayDeviatedGoal=Integer.valueOf(action.getText(GameStateInfoLocators.awayDeviatedGoal));
        /** 主队球权 */
        double homeBallright=Double.valueOf(action.getText(GameStateInfoLocators.homeBallright));
        /** 客队球权 */
        double awayBallright=Double.valueOf(action.getText(GameStateInfoLocators.awayBallright));
        /** 开赛时间 */
        String lstime=action.getText(GameStateInfoLocators.lsTime);
        /** 主队名*/
        String zdName=action.getText(GameStateInfoLocators.zdName);
        /**客队名*/
        String kdName=action.getText(GameStateInfoLocators.kdName);
        gameStateInfo.setHomeScoreHome(homeScoreHome);
        gameStateInfo.setAwayScoreAway(awayScoreAway);
        gameStateInfo.setHomeAttack(homeAttack);
        gameStateInfo.setAwayAttack(awayAttack);
        gameStateInfo.setHomeDangerousAttack(homeDangerousAttack);
        gameStateInfo.setAwayDangerousAttack(awayDangerousAttack);
        gameStateInfo.setHomeShootGoal(homeShootGoal);
        gameStateInfo.setAwayShootGoal(awayShootGoal);
        gameStateInfo.setHomeDeviatedGoal(homeDeviatedGoal);
        gameStateInfo.setAwayDeviatedGoal(awayDeviatedGoal);
        gameStateInfo.setHomeBallright(homeBallright);
        gameStateInfo.setAwayBallright(awayBallright);
        gameStateInfo.setLstime(lstime);
        gameStateInfo.setZdName(zdName);
        gameStateInfo.setKdName(kdName);
        gameStateInfo.setMsg("比赛结束");
        gameStateInfo.setIsMatchEnd("1");
        return gameStateInfo;
    }

}
