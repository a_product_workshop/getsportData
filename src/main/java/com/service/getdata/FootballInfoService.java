package com.service.getdata;

import com.bean.FootballInfo;
import com.locators.FootBallInfoLocators;
import com.service.webdriver.Browser;
import com.service.webdriver.ElementAction;
import com.utils.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/3/15.
 */
public class FootballInfoService extends Browser{
    ElementAction action=new ElementAction();
   Log log=new Log(this.getClass());


    /**
     * 通过联赛名，主对名，客队名获取 单场足球赛事信息
     * @param footballInfo
     * @return
     */
    public FootballInfo getSingleFootBallInfo(FootballInfo footballInfo){
      String lsName=footballInfo.getLsname();
      String zdName=footballInfo.getZdname();
      String kdName=footballInfo.getKdname();
     List<FootballInfo> footballInfos=getAllFootBallInfo();
        for (int i=0;i<footballInfos.size();i++)
        {
            FootballInfo footballInfo1=footballInfos.get(i);
            if ((lsName+zdName+kdName).equals(footballInfo1.getLsname()+footballInfo1.getZdname()+footballInfo1.getKdname())){
                footballInfo=footballInfo1;
                break;
            }
        }
        return footballInfo;
    }
    /**
     * 返回所有联赛信息
     * @return
     */
    public List<FootballInfo> getAllFootBallInfo()
    {
        try {
            action.sleep(500);
            return getAllFootBallInfoFromOnline();
        }catch (StaleElementReferenceException e){
            action.sleep(500);
            return getAllFootBallInfoFromOnline();
        }
    }

    /**
     * 获取当前时刻全部联赛信息
     * @return
     */
    public List<FootballInfo> getAllFootBallInfoFromOnline(){
        List<FootballInfo> footballInfos=new ArrayList<>();
        //获取联赛容器列表
        List<WebElement> lsContainers=action.findElements(FootBallInfoLocators.lsContainer);
        log.info("当前查找到"+lsContainers.size()+"场联赛");
        /*** 遍历联赛*/
        for (int i=0;i<lsContainers.size();i++)
        {
            String lsNameXpath=FootBallInfoLocators.lsContainer+"["+(i+1)+"]"+FootBallInfoLocators.lsName;
            String lsName=action.getText(lsNameXpath);
            String ccContainerXpath=FootBallInfoLocators.lsContainer+"["+(i+1)+"]"+FootBallInfoLocators.ccContainer;
            //获取场次容器列表
            List<WebElement> ccContainers=action.findElements(ccContainerXpath);
            log.info("第"+(i+1)+"场联赛，"+lsName+"查找到"+ccContainers.size()+"场比赛");
            /*** 遍历场次 */
            for (int j=0;j<ccContainers.size();j++)
            {
                String zdNameXpath="(("+FootBallInfoLocators.lsContainer+"["+(i+1)+"]"+FootBallInfoLocators.ccContainer+")"+"["+(j+1)+"]"+FootBallInfoLocators.qdname+")[1]";
                String kdNameXpath="(("+FootBallInfoLocators.lsContainer+"["+(i+1)+"]"+FootBallInfoLocators.ccContainer+")"+"["+(j+1)+"]"+FootBallInfoLocators.qdname+")[2]";
                String lsTimeXpath="("+FootBallInfoLocators.lsContainer+"["+(i+1)+"]"+FootBallInfoLocators.ccContainer+")"+"["+(j+1)+"]"+FootBallInfoLocators.lstime;
                String seeMatchInfoXpath="("+FootBallInfoLocators.lsContainer+"["+(i+1)+"]"+FootBallInfoLocators.ccContainer+")"+"["+(j+1)+"]"+FootBallInfoLocators.seeMatchInfo;
                String zdName=action.getText(zdNameXpath);
                String kdName=action.getText(kdNameXpath);
                String lsTime=action.getText(lsTimeXpath);
                FootballInfo footballInfo=new FootballInfo();
                footballInfo.setZdname(zdName);
                footballInfo.setLsname(lsName);
                footballInfo.setKdname(kdName);
                footballInfo.setLstime(lsTime);
                if (lsTime.equals("-1")){
                    footballInfo.setMsg("当前比赛已结束");
                }
                footballInfo.setSeeSingleMatchButtonXpath(seeMatchInfoXpath);
                footballInfos.add(footballInfo);
            }

        }
        log.info("总共查找到"+footballInfos.size()+"比赛");
//        footballInfos.contains()
        return footballInfos;
    }
}
